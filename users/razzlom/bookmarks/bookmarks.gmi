# RaZZlom's Bookmarks

## Gemini

=> gemini://anime.flounder.online/
=> gemini://dimension.sh/~novaburst/
=> gemini://drewdevault.com/
=> gemini://envs.net/~novaburst/
=> gemini://gemini.bortzmeyer.org/software/lupa/stats.gmi
=> gemini://gemini.circumlunar.space/
=> gemini://gemini.circumlunar.space/~solderpunk/
=> gemini://gemini.cyberbot.space/
=> gemini://gemini.quietplace.xyz/
=> gemini://geminispace.info/
=> gemini://gmi.bacardi55.io/
=> gemini://hugeping.ru/
=> gemini://hugeping.tk/
=> gemini://kirill.zholnay.name/
=> gemini://mo.rijndael.cc/
=> gemini://phreedom.club/
=> gemini://phreedom.club/~tolstoevsky/
=> gemini://rawtext.club/
=> gemini://rawtext.club/~ploum/
=> gemini://rawtext.club/~tolstoevsky/
=> gemini://republic.circumlunar.space/zine/index.gmi
=> gemini://skyjake.fi/
=> gemini://stalker.shpakovsky.ru/
=> gemini://topotun.dynu.com/
=> gemini://topotun.hldns.ru/index.gmi

### Tinylogs

=> gemini://gemini.quietplace.xyz/~razzlom/tinylog/tinylog.gmi
=> gemini://gmi.bacardi55.io/tinylog.gmi
=> gemini://phreedom.club/~tolstoevsky/tinylog.gmi
=> gemini://tinylogs.gmi.bacardi55.io/index.gmi

### Aggregators

=> gemini://calcuode.com/gmisub-aggregate.gmi
=> gemini://feeds.gmi.bacardi55.io/
=> gemini://gemini.circumlunar.space/capcom
=> gemini://nytpu.com/feed.gmi
=> gemini://phreedom.club/gmisub
=> gemini://rawtext.club/~sloum/spacewalk.gmi
=> gemini://skyjake.fi/~Cosmos/
=> gemini://warmedal.se/~antenna/

### Text game

=> gemini://warmedal.se/~bjorn/fahrenheit-outpost/

## Gopher

=> gopher://zaibatsu.circumlunar.space/1/~solderpunk/

### Aggregators

=> gopher://i-logout.cz:70/1/bongusta/

## WWW

=> https://10kbclub.com/
=> https://1mb.club/
=> https://250kb.club/
=> https://512kb.club/
=> https://codeberg.org/bacardi55/gemini-tinylog-rfc
=> https://codeberg.org/oppenlab/gempub
=> https://github.com/bacardi55/gtl/
=> https://neustadt.fr/
=> https://nocss.club/
=> https://simpleweb.iscute.ovh/
=> https://textboard.org/
=> https://tildegit.org/ploum/offpunk
=> https://tildegit.org/solderpunk/molly-brown

