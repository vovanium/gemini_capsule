# Visual Novels

Hello my dear stranger, i wrote this words for my friend Gakamine [1], so you can read it too.

=> https://antabaka.me/@Gakamine [1] Gakamine

Send me your feedback to my Fediverse account [2].

=> https://quietplace.xyz/@razzlom [2] Fediverse account

Thank you for reading, I'm hope you liked it.

## Beskonechnoe leto

=> https://vndb.org/v3126

VN made on russian imgeboard https://iichan.hk [3] about Semyon who travels in time and place to USSR pioneer summer camp.

=> https://iichan.hk [3] https://iichan.hk

Story not very interesting, but backgrounds, characters and music beautiful! This VN has place in my heart. Foreigners might not like it.

## Bulki, kefir i rok-n-roll

=> https://vndb.org/v22866

Best mod for Beskonechnoe leto. All i wrote earlier count and for this VN.

## Carpe Diem

=> https://vndb.org/v18719

Very short and cute VN about dating with girl. Not very interesting.

## Coffee Talk

=> https://vndb.org/v23634

Beautiful and interesting pixel art VN.

You are barista, you listening to strangers stories, gave them drinks and helping with advises.

I'm trying to create game atmosphere at https://quietplace.xyz [4], but i don't think i can manage it.

=> https://quietplace.xyz [4] https://quietplace.xyz

I'm recommend this VN for relaxing with cup of hot chocolate.

## Crimson Gray

=> https://vndb.org/v21499

VN from yandere lover for yandere lovers. For me this VN for one time walkthrough.

## Crystal the Witch

=> https://vndb.org/v26843

VN about silly witch and cyberbulling. You can skip it.

## Dangan Ronpa Kibou no Gakuen to Zetsubou no Koukousei

=> https://vndb.org/v7014

VN with gameplay about hope, desperation and murders.

If you want hard atmosphere like in Fata morgana no Yakata i think you like this VN.

## Doki Doki Literature Club!

=> https://vndb.org/v21905

VN starting like normal datingsim, but then happening murder. Breaking fourth wall best part of VN.

## Fata morgana no Yakata

=> https://vndb.org/v12402

You played it right now. You all know about it.

If you want more Fata Morgana you can try Fata Morgana no Yakata -Another Episodes- [5] , but i don't read it and can't say anything.

=> https://vndb.org/v18397 [5] https://vndb.org/v18397

## Kara no Shoujo

=> https://vndb.org/v810

You are a detective who investigates a series of murders in Tokyo in 1956. VN has slow and depressing atmosphere but i don't like ending.

Story interesting, you can try it.

## Katawa Shoujo

=> https://vndb.org/v945

4chan game about cute disabled girls. Read if you like datingsims and nude girls.

## Moe Era

=> https://vndb.org/v26814

```
Description

So, what is Moe Era about?
Why, it's about cute things and having fun!
And, of course, a deep, intriguing story!
A story that makes you think...
```

VN from russian developers. Official description fully describes this VN.

## Moroznyj Poceluj

=> https://vndb.org/v16637 

Short Christmas story with characters from Beskonechnoe leto.

## Neko Para

=> https://vndb.org/v22020 Neko Para Extra: Koneko no Hi no Yakusoku
=> https://vndb.org/v17763 Neko Para Vol.0 Minazuki Neko-tachi no Nichijou!
=> https://vndb.org/v15538 Neko Para Vol.1 Soleil Kaiten Shimashita!
=> https://vndb.org/v18713 Neko Para Vol.2 Shimai Neko no Sucre
=> https://vndb.org/v19385 Neko Para Vol.3 Neko-tachi no Aromatize

So, Neko Para, huh. All VNs about cute nekogirls and sex with them. Read if you can't live without nekogirls.

## Saya no Uta

=> https://vndb.org/v97

This my second recommendation to you after Danganronpa.

Romance story about boy and loli-monster with tough ending. I liked it!

## Tsukihime

=> https://vndb.org/v7

My first TypeMoon VN. Almost two separate stories in one game. Not bad.

